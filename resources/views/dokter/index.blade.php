@extends('layout.master')

@section('title')
  List Dokter
@endsection

@section('content')
<a href="/dokter/create" class="btn btn-primary my-3">Tambah</a>
  <div class="row">
    @foreach ($data as $key=>$value)
    <div class="col-3">
      <div class="card" style="width:15rem;">
        <img class="card-img-top" src="{{asset('images/'.$value->foto)}}" alt="Card image cap" style="min-height:300px;max-height:300px;">
        <div class="card-body">
          <h4 class="card-title"><b>{{$value->nama}}</b></h4>
          <p class="card-text" style="text-align:justify;min-height:100px;max-height:100px;">{!! nl2br(e(Str::limit($value->bio,100)))!!}</p>
          <form action="/dokter/{{$value->id}}" method="POST">
            <a href="/dokter/{{$value->id}}" class="btn btn-info">Lihat</a>
            <a href="/dokter/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Hapus">
          </form>
        </div>
      </div>
    </div>
    @endforeach
  </div>       
@endsection
