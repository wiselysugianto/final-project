@extends('layout.master')

@section('title')
  Lihat Dokter
@endsection

@section('content')
<img src="{{asset('images/'.$data->foto)}}" height="300"></img><br>
<h4>Nama: {{$data->nama}}</h4>
Tanggal Lahir: {{$data->tgl_lahir}}<br>
{!! nl2br(e($data->bio)) !!}
@endsection