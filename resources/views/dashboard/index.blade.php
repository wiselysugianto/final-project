@extends('layout.master')

@section('title')
  Dashboard
@endsection

@section('content')
  <img src="{{asset('images/ERD.png')}}"></img>
@endsection

@push('script')
  <script>
    Swal.fire({
    icon: 'success',
    title: 'Login Berhasil!',
    text: 'Selamat datang {{Auth::user()->name}}',});
  </script>
@endpush