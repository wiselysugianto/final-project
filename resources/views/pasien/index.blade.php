@extends('layout.master')

@section('title')
  List Pasien
@endsection

@section('content')
<a href="/pasien/create" class="btn btn-primary my-3">Tambah</a>
<table class="table" id="example1">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Keluhan</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->tgl_lahir}}</td>
                <td>{!! nl2br(e($value->keluhan)) !!}</td>
                <td>
                  <form action="/pasien/{{$value->id}}" method="POST">
                    <a href="/pasien/{{$value->id}}" class="btn btn-info">Lihat</a>
                    <a href="/pasien/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Hapus">
                  </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>Tidak ada data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection

@push('script')
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush