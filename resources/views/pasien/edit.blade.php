@extends('layout.master')

@section('title')
  Edit Pasien
@endsection

@section('content')
<div>
  <form action="/pasien/{{$data->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="title">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" value="{{$data->nama}}" placeholder="Nama Pasien">
      @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
     <div class="form-group">
      <label for="title">Tgl Lahir</label>
      <div class="input-group date" id="tgl_lahir" data-target-input="nearest">
        <input type="text" class="form-control datetimepicker-input" name="tgl_lahir" data-target="#tgl_lahir" value="{{$data->tgl_lahir}}" data-toggle="datetimepicker" placeholder="Tanggal Lahir"/>
        <div class="input-group-append" data-target="#tgl_lahir" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
    @error('tgl_lahir')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
    @enderror
    <div class="form-group">
      <label for="title">Keluhan</label>
      <textarea rows="5" class="form-control" name="keluhan" id="keluhan" placeholder="Keluhan">{{$data->keluhan}}</textarea>
      @error('keluhan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection

@push('script')
<script>
  //Date picker
   $(document).ready(function(){
    $('#tgl_lahir').datetimepicker({
      format: 'YYYY-MM-DD',
    });
  });
</script>
@endpush