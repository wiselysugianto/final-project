@extends('layout.master')

@section('title')
  List Jadwal
@endsection

@section('content')
<a href="/jadwal/create" class="btn btn-primary my-3">Tambah</a>
<table class="table" id="example1">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Dokter</th>
      <th scope="col">Pasien</th>
      <th scope="col">Tanggal Check Up</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($data as $key=>$value)
        <tr>
          <td>{{$key + 1}}</th>
          <td>{{$value->dokter->nama}}</td>
          <td>{{$value->pasien->nama}}</td>
          <td>{{$value->tgl_checkup}}</td>
          <td>
            <form action="/jadwal/{{$value->id}}" method="POST">
              <a href="/jadwal/{{$value->id}}" class="btn btn-info">Lihat</a>
              <a href="/jadwal/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
              @csrf
              @method('DELETE')
              <input type="submit" class="btn btn-danger my-1" value="Hapus">
            </form>
          </td>
      </tr>
    @empty
      <tr colspan="3">
          <td>Tidak ada data</td>
      </tr>  
    @endforelse              
  </tbody>
</table>
@endsection

@push('script')
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush