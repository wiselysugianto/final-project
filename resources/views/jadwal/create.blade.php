@extends('layout.master')

@section('title')
  Tambah Jadwal
@endsection

@section('content')
<div>
  <form action="/jadwal" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="title">Dokter</label>
      <select class="form-control" name="dokter_id" id="dokter_id" placeholder="Dokter">
        <option value="">-- Kosong --</option>
        @foreach($dokter as $key=>$value)
        <option value="{{$value->id}}">{{$value->nama}}</option>
        @endforeach
      </select>
      @error('dokter')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <div class="form-group">
      <label for="title">Pasien</label>
      <select class="form-control" name="pasien_id" id="pasien_id" placeholder="Pasien">
        <option value="">-- Kosong --</option>
        @foreach($pasien as $key=>$value)
        <option value="{{$value->id}}">{{$value->nama}}</option>
        @endforeach
      </select>
      @error('pasien')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <div class="form-group">
      <label for="title">Tgl Check Up</label>
      <div class="input-group date" id="tgl_checkup" data-target-input="nearest">
        <input type="text" class="form-control datetimepicker-input" name="tgl_checkup" data-target="#tgl_checkup" data-toggle="datetimepicker" placeholder="Tanggal Check Up"/>
        <div class="input-group-append" data-target="#tgl_checkup" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
    @error('tgl_checkup')
      <div class="alert alert-danger">
          {{ $message }}
      </div>
    @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection

@push('script')
<script>
  //Date picker
   $(document).ready(function(){
    $('#tgl_checkup').datetimepicker({
      format: 'YYYY-MM-DD',
    });
  });
</script>
@endpush