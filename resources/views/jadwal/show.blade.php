@extends('layout.master')

@section('title')
  Lihat Jadwal
@endsection

@section('content')
<h4>Dokter: {{$data->dokter->nama}}</h4>
<h4>Pasien: {{$data->pasien->nama}}</h4>
Tanggal Check Up: {{$data->tgl_checkup}}<br>
{!! nl2br(e($data->pasien->keluhan)) !!}
@endsection