<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $fillable = ['dokter_id','pasien_id','tgl_checkup',];

    public function dokter(){
        return $this->belongsTo('App\Dokter');
    }

    public function pasien(){
        return $this->belongsTo('App\Pasien');
    }
}
