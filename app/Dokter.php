<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter';
    protected $fillable = ['nama','tgl_lahir','jns_kel','bio','foto',];

    public function jadwal(){
        return $this->hasMany('App\Jadwal');
    }
}
