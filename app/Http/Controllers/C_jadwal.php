<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\Dokter;
use App\Pasien;

class C_jadwal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Jadwal::all();
		return view('jadwal.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $dokter = Dokter::all();
        $pasien = Pasien::all();
        return view('jadwal.create',compact('dokter','pasien'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
			'dokter_id'    => 'required',
            'pasien_id'    => 'required',
            'tgl_checkup'  => 'required',
		]);

        $jadwal = new Jadwal;
        $jadwal->dokter_id = $request->dokter_id;
        $jadwal->pasien_id = $request->pasien_id;
        $jadwal->tgl_checkup = $request->tgl_checkup;
        $jadwal->save();
           
        return redirect('/jadwal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Jadwal::where('id', $id)->first();
		return view('jadwal.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokter = Dokter::all();
        $pasien = Pasien::all();
        $data = Jadwal::where('id', $id)->first();
		return view('jadwal.edit', compact('data','dokter','pasien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
			'dokter_id'    => 'required',
            'pasien_id'    => 'required',
            'tgl_checkup'  => 'required',
		]);

        $jadwal = Jadwal::find($id);
        $jadwal->dokter_id = $request->dokter_id;
        $jadwal->pasien_id = $request->pasien_id;
        $jadwal->tgl_checkup = $request->tgl_checkup;
        $jadwal->save();
           
        return redirect('/jadwal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = Jadwal::findorfail($id);
        $jadwal->delete();
        return redirect('/jadwal');
    }
}
