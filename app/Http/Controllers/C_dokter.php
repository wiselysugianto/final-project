<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;
use File;

class C_dokter extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dokter::all();
		return view('dokter.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dokter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
			'nama'        => 'required|max:50',
            'tgl_lahir'   => 'required',
            'bio'         => 'required|max:65535',
            'foto'        => 'required|mimes:jpeg,bmp,png',
		]);

        $gambar = $request->foto;
        $name_img = time().' - '.$gambar->getClientOriginalName();
        $path = 'images';

        $dokter = new Dokter;
        $dokter->nama = $request->nama;
        $dokter->tgl_lahir = $request->tgl_lahir;
        $dokter->bio = $request->bio;
        $dokter->foto = $name_img;
        $dokter->save();
        
        $gambar->move($path,$name_img);    
        return redirect('/dokter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Dokter::where('id', $id)->first();
		return view('dokter.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Dokter::where('id', $id)->first();
        return view('dokter.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
			'nama'        => 'required|max:50',
            'tgl_lahir'   => 'required',
            'bio'         => 'required|max:65535',
            'foto'        => 'mimes:jpeg,bmp,png',
		]);
         
        $dokter = Dokter::find($id);
        $dokter->nama = $request->nama;
        $dokter->tgl_lahir = $request->tgl_lahir;
        $dokter->bio = $request->bio;

        if($request->has('foto')){
            // hapus dulu gambar lama
            $path = 'images/';
            $data_lama = Dokter::where('id', $id)->first();
            File::delete($path.$data_lama->foto);

            // upload gambar baru
            $gambar = $request->foto;
            $name_img = time().' - '.$gambar->getClientOriginalName();
            $path = 'images';

            $dokter->foto = $name_img;
            $dokter->save();

            $gambar->move($path,$name_img);
        }else{
            $dokter->save();
        }
        
        return redirect('/dokter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokter = Dokter::findorfail($id);
        $dokter->delete();

        $path = 'images/';
        File::delete($path.$dokter->foto);
        return redirect('/dokter');
    }
}
