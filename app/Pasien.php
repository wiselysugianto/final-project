<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';
    protected $fillable = ['nama','tgl_lahir','keluhan',];

    public function jadwal(){
        return $this->hasMany('App\Jadwal');
    }
}
