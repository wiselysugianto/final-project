<?php $__env->startSection('title'); ?>
  List Pasien
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<a href="/pasien/create" class="btn btn-primary my-3">Tambah</a>
<table class="table" id="example1">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Tanggal Lahir</th>
        <th scope="col">Keluhan</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php $__empty_1 = true; $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <tr>
                <td><?php echo e($key + 1); ?></th>
                <td><?php echo e($value->nama); ?></td>
                <td><?php echo e($value->tgl_lahir); ?></td>
                <td><?php echo nl2br(e($value->keluhan)); ?></td>
                <td>
                  <form action="/pasien/<?php echo e($value->id); ?>" method="POST">
                    <a href="/pasien/<?php echo e($value->id); ?>" class="btn btn-info">Lihat</a>
                    <a href="/pasien/<?php echo e($value->id); ?>/edit" class="btn btn-primary">Edit</a>
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('DELETE'); ?>
                    <input type="submit" class="btn btn-danger my-1" value="Hapus">
                  </form>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <tr colspan="3">
                <td>Tidak ada data</td>
            </tr>  
        <?php endif; ?>              
    </tbody>
</table>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('script'); ?>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\sanbercode\laravel\pekan4\final-project\resources\views/pasien/index.blade.php ENDPATH**/ ?>