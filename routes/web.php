<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']],function(){
    Route::get('/', 'C_dashboard@index');
    
    // dokter
    Route::resource('dokter','C_dokter');

    // pasien
    Route::resource('pasien','C_pasien');
    
    // jadwal
    Route::resource('jadwal','C_jadwal');

    // Route::get('/home', 'HomeController@index')->name('home');
});

Auth::routes();